<?php

class Point
{
	public $x;
	public $y;
	public $type;

	function __construct($x, $y, $type = "")
	{
		$this->x = $x;
		$this->y =  $y;
		$this->type = $type;
	}

	function t()
	{
		return $this->x . ',' . $this->y . ' '; 
	}
}

class Matrix
{
	public $a;
	public $b;
	public $c;
	public $d;
	public $e;
	public $f;

	function __construct($a = 1,$b = 0,$c = 0,$d = 1,$e = 0,$f = 0)
	{
		$this->a = $a;
		$this->b = $b;
		$this->c = $c;
		$this->d = $d;
		$this->e = $e;
		$this->f = $f;
	}

	function scale($sx, $sy)
	{
		$o = new Matrix($sx, 0, 0, $sy, 0, 0);
		$this->mult($o);
	}

	function translate($tx, $ty)
	{
		$o = new Matrix(1, 0, 0, 1, $tx, $ty);
		$this->mult($o);
	}
	

	function point($p)
	{
		$r = new Point($p->x, $p->y, $p->type);
		$r->x = ($p->x * $this->a) + $this->e;
		$r->y = ($p->y * $this->d) + $this->f;
		return $r;
	}

	function mult($m)
	{
		// does not support rotation yet
		$this->a = $this->a * $m->a;
		$this->d = $this->d * $m->d;
		$this->e = ($this->a * $m->e) + $this->e;
		$this->f = ($this->d * $m->f) + $this->f;
	}

	function t()
	{
		return $this->a.' '.$this->b.' '.$this->c.' '.$this->d.' '.$this->e.' '.$this->f;
	}

}

class UFOGlifs
{
	private $ufo;
	private $glist = array();

	function __construct($ufodir)
	{
		$this->ufo = $ufodir;
		$plist = simplexml_load_file($ufodir . '/glyphs/contents.plist');
		$plen = count($plist->dict->key);
		//var_dump($plen);
		for($i = 0; $i < $plen; $i++)
		{
		//	var_dump($plist->dict->key[$i]);
			$this->glist[(string)$plist->dict->key[$i]] = (string)$plist->dict->string[$i];
		}
		//var_dump($this);
	}

	function getList()
	{
		return array_keys($this->glist);
	}

	function glyph($name, $scale = 1)
	{
		$baseMatrix = new Matrix();
		$baseMatrix->translate(0,800 * $scale);
		$baseMatrix->scale($scale, $scale);

		if(!isset($this->glist[$name]))
			return '<h1>ERROR: '.$name.' Does not exist</h1>';
	
		$g = simplexml_load_file($this->ufo . '/glyphs/' . $this->glist[$name] );
		$xadvance = ((string)$g->advance['width']);
		$svg = '<svg:svg width="'.floor($xadvance * $scale).'px" height="'.floor(1000 * $scale).'px">';
		$svg .= "\n".'<svg:path style="fill:#333;fill-rule:nonzero;stroke:none;stroke-width:0;" d="';
		$close = true;
		$contours = array();
		foreach($g->outline->component as $co)
		{
			$cx = simplexml_load_file($this->ufo . '/glyphs/' . $this->glist[(string)$co['base']] );
			$sx = isset($co['xScale']) ? $co['xScale'] : 1 ;
			$sy = isset($co['yScale']) ? $co['yScale'] : 1 ;
			$tx = isset($co['xOffset']) ? $co['xOffset'] : 0 ;
			$ty = isset($co['yOffset']) ? $co['yOffset'] : 0 ;
			$m = new Matrix();
			$m->translate($tx * $scale,(800 - $ty)  * $scale);
			$m->scale($sx * $scale, $sy * $scale);
			echo '
			<!-- 
			B ['.$baseMatrix->t().']
			M ['.$m->t().']
			-->
			';
			foreach($cx->outline->contour as $c)
			{
				$contours[] = array("contour" => $c, "matrix" => $m);
			}
		}
		foreach($g->outline->contour as $c)
		{
			$contours[] = array("contour" => $c, "matrix" => $baseMatrix);
		}
		foreach($contours as $cont)
		{
			$c = $cont['contour'];
			$m = $cont['matrix'];
			$firstmove = null;
			$points = array();
			foreach($c->point as $p)
			{
				$points[] = $m->point(new Point((string)$p['x'], -(string)$p['y'], (string)$p['type']));
			}

			if($points[0]->type == 'curve')
			{
				$c = array_shift($points);
				$points[] = $c;
			}

			if(($firstmove == null) && ($points[0]->type != 'move'))
			{
				$firstmove = array_pop($points);
				$points[] = $firstmove;
				$svg .= 'M'.$firstmove->t();
			}
		
			$plen = count($points);
			for($i = 0; $i < $plen; $i++)
			{
				if($points[$i]->type == 'curve')
				{
					$svg .= 'C'.$points[$i - 2]->t();
					$svg .= $points[$i-1]->t();
					$svg .= $points[$i]->t();
					//$close = true;
				}
				else if($points[$i]->type == 'line')
				{
					$svg .= 'L'.$points[$i]->t();
					//$close = false;
				}
				else if($points[$i]->type == 'move') // we expect the UFO to be well formed, no check it's first point
				{
					$svg .= 'M'.$points[$i]->t();
					$firstmove = $points[$i];
					$close = false;
				}
			}
			if($close)
				$svg .= 'z ';

		}
		$svg .= '"/>';
		$svg .= '<svg:path transform="scale('.$scale.')" style="fill:none;stroke:blue;stroke-width:1px;" 
			d="M0,800 L'.$xadvance.',800"/>';
		$svg .= '</svg:svg>';
		return $svg;
	}
}

$UFO = new UFOGlifs($_POST['ufo']);

if(isset($_POST['list']) && $_POST['list'] == '1')
{
	$l = $UFO->getList();
	echo '<div class="glist">';
	foreach($l as $e)
	{
		echo '<div class="glist_elem" id="'.$e.'"> '.$e.',</div>';
	}
	echo '</div>';
}
else
{
$gname = $_POST['g'];
header('Content-type: application/xhtml+xml');
echo $UFO->glyph($gname, 0.6);
}

